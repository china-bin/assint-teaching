/*
 Navicat Premium Data Transfer

 Source Server         : 本地mysql
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : assint_teaching

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 14/06/2020 20:43:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chatbot
-- ----------------------------
DROP TABLE IF EXISTS `chatbot`;
CREATE TABLE `chatbot`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gmt_create` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `status_id` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(1:正常，0:禁用)',
  `question` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题',
  `answer` varchar(2555) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '答案',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chatbot
-- ----------------------------
INSERT INTO `chatbot` VALUES (1, '2020-05-11 13:46:21', '2020-05-11 13:46:21', 1, 'java是什么', 'Java是一门面向对象编程语言，不仅吸收了C++语言的各种优点，还摒弃了C++里难以理解的多继承、指针等概念，因此Java语言具有功能强大和简单易用两个特征。');
INSERT INTO `chatbot` VALUES (2, '2020-05-11 13:46:21', '2020-05-11 13:46:21', 1, '你是谁', '我是在线教学平台的对话机器人');
INSERT INTO `chatbot` VALUES (3, '2020-05-12 21:04:02', '2020-05-12 21:04:02', 1, '你爸爸是谁', '我爸爸是最帅气最牛逼的人');
INSERT INTO `chatbot` VALUES (4, '2020-05-12 21:35:34', '2020-05-12 21:35:34', 1, '我爱你', '我也爱你宝贝');
INSERT INTO `chatbot` VALUES (5, '2020-05-12 21:36:16', '2020-05-12 21:36:16', 1, '我想你了', '宝贝乖，我马上回家！');
INSERT INTO `chatbot` VALUES (6, '2020-05-12 21:37:13', '2020-05-12 21:37:13', 1, '你在干嘛', '我在学习呢');
INSERT INTO `chatbot` VALUES (7, '2020-05-12 21:38:46', '2020-05-12 21:38:46', 1, 'spring cloud是什么', 'Spring Cloud是一个微服务框架,相比Dubbo等RPC框架, Spring Cloud提供的全套的分布式系统解决方案。');
INSERT INTO `chatbot` VALUES (8, '2020-05-12 21:40:40', '2020-05-12 21:40:40', 1, 'spring cloud有什么优势', 'Spring Cloud 来源于 Spring，质量、稳定性、持续性都可以得到保证。\r\nSpirng Cloud 天然支持 Spring Boot，更加便于业务落地。\r\nSpring Cloud 发展非常的快，从 2016 年开始接触的时候相关组件版本为 1.x，到现在将要发布 2.x 系列。\r\nSpring Cloud 是 Java 领域最适合做微服务的框架。\r\n相比于其它框架，Spring Cloud 对微服务周边环境的支持力度最大。\r\n对于中小企业来讲，使用门槛较低。\r\nSpring Cloud 是微服务架构的最佳落地方案。');
INSERT INTO `chatbot` VALUES (9, '2020-05-12 22:06:27', '2020-05-12 22:06:27', 1, '你多大了', '宝宝现在才刚出生');
INSERT INTO `chatbot` VALUES (10, '2020-05-12 22:07:53', '2020-05-12 22:07:53', 1, '你妈妈是谁', '宝宝现在暂时还没有妈妈，如果你有意向做我妈妈，请联系qq:xxxxxxxxxx');
INSERT INTO `chatbot` VALUES (11, '2020-05-13 09:49:22', '2020-05-13 09:49:22', 1, 'hi', 'hello');
INSERT INTO `chatbot` VALUES (12, '2020-05-13 09:51:43', '2020-05-13 09:51:43', 1, 'spring boot是什么', 'Spring Boot提供了一种新的编程范式，能在最小的阻力下开发Spring应用程序。有了它， 你可以更加敏捷地开发Spring应用程序，专注于应用程序的功能，不用在Spring的配置上多花功 夫，甚至完全不用配置。实际上，Spring Boot的一项重要工作就是让Spring配置不再成为你成功路上的绊脚石。');
INSERT INTO `chatbot` VALUES (13, '2020-05-21 22:15:03', '2020-05-21 22:15:03', 1, '你好', '你也好');

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题类型：1表示单选，2表示多选，3表示填空，4表示判断，5表示问答',
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '题干',
  `option_a` varchar(5120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'A选项答案',
  `option_b` varchar(5120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'B选项答案',
  `option_c` varchar(5120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'C选项答案',
  `option_d` varchar(5120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D选项答案',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '答案',
  `analyse` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '解析',
  `score` decimal(10, 0) NULL DEFAULT NULL COMMENT '该题的分数',
  `create_by` int(11) NULL DEFAULT NULL,
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL,
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '问题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of question
-- ----------------------------
INSERT INTO `question` VALUES (22, '1', '关于sleep()和wait()，以下描述错误的一项是？', 'sleep是线程类（Thread）的方法，wait是Object类的方法； ', 'sleep不释放对象锁，wait放弃对象锁； ', 'sleep暂停线程、但监控状态仍然保持，结束后会自动恢复；', 'wait后进入等待锁定池，只有针对此对象发出notify方法后获得对象锁进入运行状态。', 'D', 'Notify后是进入对象锁定池，准备获得锁，而不是立即获得。', 5, NULL, '2019-05-08 23:39:53', NULL, NULL);
INSERT INTO `question` VALUES (23, '1', '提供Java存取数据库能力的包是？', 'java.sql ', 'java.awt ', 'java.lang ', 'java.swing', 'A', NULL, 5, NULL, '2019-05-08 23:40:28', NULL, NULL);
INSERT INTO `question` VALUES (24, '1', '方法resume()负责恢复哪些线程的执行？', '通过调用stop()方法而停止的线程。 ', '通过调用sleep()方法而停止的线程。', '通过调用wait()方法而停止的线程。 ', '通过调用suspend()方法而停止的线程。', 'D', NULL, 5, NULL, '2019-05-08 23:41:03', NULL, NULL);
INSERT INTO `question` VALUES (25, '1', 'Java I/O程序设计中，下列描述正确的是', 'OutputStream用于写操作 ', 'InputStream用于写操作 ', 'I/O库不支持对文件可读可写API', '以上都对', 'A', NULL, 5, NULL, '2019-05-08 23:41:54', NULL, NULL);
INSERT INTO `question` VALUES (29, '1', '分析选项中关于Java中this关键字的说法正确的是', 'this关键字是在对象内部指代自身的引用 ', ' this关键字可以在类中的任何位置使用 ', 'this关键字和类关联，而不是和特定的对象关联 ', '同一个类的不同对象共用一个this', 'A', NULL, 5, NULL, '2019-05-08 23:44:20', NULL, NULL);
INSERT INTO `question` VALUES (30, '1', '在JAVA中，LinkedList类和ArrayList类同属于集合框架类，下列（ ）选项中的方法是LinkedList类有而ArrayList类没有的。', ' add(Object o) ', ' add(int index，Object o) ', ' remove(Object o) ', 'removeLast()', 'D', NULL, 5, NULL, '2019-05-08 23:46:28', NULL, NULL);
INSERT INTO `question` VALUES (31, '1', '在JAVA中ArrayList类实现了可变大小的数组，便于遍历元素和随机访问元素，已知 获得了ArrayList类的对象bookTypeList，则下列语句中能够实现判断列表中是否存在字符串“小说”的', '基本数据类型和String相加结果一定是字符串型 ', 'char类型和int类型相加结果一定是字符 ', 'double类型可以自动转换为int ', ' char + int + double +”” 结果一定是double', 'A', NULL, 5, NULL, '2019-05-08 23:47:20', NULL, NULL);
INSERT INTO `question` VALUES (32, '1', '对象的特征在类中表示为变量，称为类的', '对象 ', '属性', '方法 ', '数据类型', 'B', NULL, 5, NULL, '2019-05-08 23:47:58', NULL, '2019-05-08 23:48:24');
INSERT INTO `question` VALUES (33, '1', '在Java中,关于构造方法，下列说法错误的是', '构造方法的名称必须与类名相同 ', ' 构造方法可以带参数 ', '构造方法不可以重载 ', '构造方法绝对不能有返回值', 'C', NULL, 5, NULL, '2019-05-08 23:48:52', NULL, NULL);
INSERT INTO `question` VALUES (34, '2', '下面哪几个函数是public void example(){…}的重载函数（  ），选两项', 'public void example( int m){…}', 'public int example(){…}', 'public void example2(){…}', 'public int example ( int m, float f){…}', 'A,D,', '方法名一定相同，参数签名（参数数量，类型，顺序）一定不同。', 10, NULL, '2019-05-08 23:49:38', NULL, NULL);
INSERT INTO `question` VALUES (35, '2', '已知如下定义：String s = “story”; 下面哪个表达式是合法的（ ），选两项', ' s += “books”', 'char c = s[1];', 'int len = s.length;', 'String t = s.toLowerCase();', 'A,D,', 'String类型没有length属性，这个专属于数组。', 10, NULL, '2019-05-08 23:51:36', NULL, NULL);
INSERT INTO `question` VALUES (36, '2', ' 如下哪些字串是Java中的标识符（ ），选两项', 'fieldname', 'super', '#number', '$number', 'A,C,', '标识符以￥$_或者字母开头，不能用关键字。', 10, NULL, '2019-05-08 23:52:12', NULL, NULL);
INSERT INTO `question` VALUES (37, '2', '如下哪些是Java中有效的关键字（ ）', ' const', 'false', 'this', 'native', 'A,B,C,D,', NULL, 10, NULL, '2019-05-08 23:52:58', NULL, NULL);
INSERT INTO `question` VALUES (38, '2', '如下哪些是Java中正确的整数表示', '22', '0x22', '022', '22H', 'A,B,C,', '从上到下依次十进制，八进制，十六进制。', 10, NULL, '2019-05-08 23:53:40', NULL, NULL);
INSERT INTO `question` VALUES (39, '5', '父类的静态方法能否被子类重写', NULL, NULL, NULL, NULL, '不能。重写只适用于实例方法,不能用于静态方法，而子类当中含有和父类相同签名的静态方法，我们一般称之为隐藏。', NULL, 20, NULL, '2019-05-08 23:55:26', NULL, NULL);
INSERT INTO `question` VALUES (40, '5', 'java 创建对象的几种方式', NULL, NULL, NULL, NULL, '采用new\n\n通过反射\n\n采用clone\n\n通过序列化机制', NULL, 20, NULL, '2019-05-08 23:55:56', NULL, NULL);
INSERT INTO `question` VALUES (41, '5', 'String s1=”ab”, String s2=”a”+”b”, String s3=”a”, String s4=”b”, s5=s3+s4请问s5==s2返回什么？', NULL, NULL, NULL, NULL, '返回false。在编译过程中，编译器会将s2直接优化为”ab”，会将其放置在常量池当中，s5则是被创建在堆区，相当于s5=new String(“ab”);', NULL, 20, NULL, '2019-05-08 23:56:05', NULL, NULL);
INSERT INTO `question` VALUES (42, '5', 'java当中的四种引用', NULL, NULL, NULL, NULL, '强引用，软引用，弱引用，虚引用。不同的引用类型主要体现在GC上:\n\n强引用：如果一个对象具有强引用，它就不会被垃圾回收器回收。即使当前内存空间不足，JVM也不会回收它，而是抛出 OutOfMemoryError 错误，使程序异常终止。如果想中断强引用和某个对象之间的关联，可以显式地将引用赋值为null，这样一来的话，JVM在合适的时间就会回收该对象。\n\n软引用：在使用软引用时，如果内存的空间足够，软引用就能继续被使用，而不会被垃圾回收器回收，只有在内存不足时，软引用才会被垃圾回收器回收。\n\n弱引用：具有弱引用的对象拥有的生命周期更短暂。因为当 JVM 进行垃圾回收，一旦发现弱引用对象，无论当前内存空间是否充足，都会将弱引用回收。不过由于垃圾回收器是一个优先级较低的线程，所以并不一定能迅速发现弱引用对象。\n\n虚引用：顾名思义，就是形同虚设，如果一个对象仅持有虚引用，那么它相当于没有引用，在任何时候都可能被垃圾回收器回收。\n\n ', NULL, 20, NULL, '2019-05-08 23:56:21', NULL, NULL);
INSERT INTO `question` VALUES (43, '5', '有没有可能两个不相等的对象有相同的hashcode', NULL, NULL, NULL, NULL, '有可能，两个不相等的对象可能会有相同的 hashcode 值，这就是为什么在 hashmap 中会有冲突。如果两个对象相等，必须有相同的hashcode 值，反之不成立。', NULL, 20, NULL, '2019-05-08 23:56:40', NULL, NULL);
INSERT INTO `question` VALUES (44, '5', '内部类的作用', NULL, NULL, NULL, NULL, '内部类可以有多个实例，每个实例都有自己的状态信息，并且与其他外围对象的信息相互独立.在单个外围类当中，可以让多个内部类以不同的方式实现同一接口，或者继承同一个类.创建内部类对象的时刻不依赖于外部类对象的创建。内部类并没有令人疑惑的”is-a”管系，它就像是一个独立的实体。\n\n内部类提供了更好的封装，除了该外围类，其他类都不能访问。', NULL, 20, NULL, '2019-05-08 23:56:56', NULL, NULL);
INSERT INTO `question` VALUES (45, '5', 'final有哪些用法', NULL, NULL, NULL, NULL, 'final也是很多面试喜欢问的地方，能回答下以下三点就不错了：\n1.被final修饰的类不可以被继承 \n2.被final修饰的方法不可以被重写 \n3.被final修饰的变量不可以被改变。如果修饰引用，那么表示引用不可变，引用指向的内容可变。\n4.被final修饰的方法，JVM会尝试将其内联，以提高运行效率 \n5.被final修饰的常量，在编译阶段会存入常量池中。\n\n回答出编译器对final域要遵守的两个重排序规则更好：\n1.在构造函数内对一个final域的写入，与随后把这个被构造对象的引用赋值给一个引用变量,这两个操作之间不能重排序。\n2.初次读一个包含final域的对象的引用，与随后初次读这个final域,这两个操作之间不能重排序。', NULL, 20, NULL, '2019-05-08 23:57:11', NULL, NULL);
INSERT INTO `question` VALUES (46, '5', '64位的JVM当中,int的长度是多少?', NULL, NULL, NULL, NULL, 'Java 中，int 类型变量的长度是一个固定值，与平台无关，都是 32 位。意思就是说，在 32 位 和 64 位 的Java 虚拟机中，int 类型的长度是相同的。', NULL, 20, NULL, '2019-05-08 23:57:26', NULL, NULL);
INSERT INTO `question` VALUES (47, '5', 'String和StringBuffer', NULL, NULL, NULL, NULL, 'String和StringBuffer主要区别是性能：String是不可变对象，每次对String类型进行操作都等同于产生了一个新的String对象，然后指向新的String对象。所以尽量不在对String进行大量的拼接操作，否则会产生很多临时对象，导致GC开始工作，影响系统性能。\n\nStringBuffer是对对象本身操作，而不是产生新的对象，因此在有大量拼接的情况下，我们建议使用StringBuffer。\n\n但是需要注意现在JVM会对String拼接做一定的优化：\nString s=“This is only ”+”simple”+”test”会被虚拟机直接优化成String s=“This is only simple test”，此时就不存在拼接过程。', NULL, 20, NULL, '2019-05-08 23:57:40', NULL, NULL);
INSERT INTO `question` VALUES (48, '5', '你知道哪些垃圾回收算法?', NULL, NULL, NULL, NULL, '垃圾回收从理论上非常容易理解,具体的方法有以下几种: \n1. 标记-清除 \n2. 标记-复制 \n3. 标记-整理 \n4. 分代回收 ', NULL, 20, NULL, '2019-05-08 23:58:01', NULL, NULL);
INSERT INTO `question` VALUES (49, '4', '覆盖的同名方法中，子类方法不能比父类方法的访问权限低', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-08 23:58:51', NULL, NULL);
INSERT INTO `question` VALUES (50, '4', '接口是特殊的类，所以接口也可以继承，子接口将继承父接口的所有常量和抽象方法。', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-08 23:59:05', NULL, NULL);
INSERT INTO `question` VALUES (51, '4', 'Java支持多重继承。', NULL, NULL, NULL, NULL, '0', NULL, 3, NULL, '2019-05-08 23:59:16', NULL, NULL);
INSERT INTO `question` VALUES (52, '4', '抽象方法没有方法体。', NULL, NULL, NULL, NULL, '0', NULL, 3, NULL, '2019-05-08 23:59:28', NULL, NULL);
INSERT INTO `question` VALUES (53, '4', '一个Java类可以有一个直接父类，并可以实现多个接口。', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-08 23:59:44', NULL, NULL);
INSERT INTO `question` VALUES (54, '4', 'final类中的属性和方法都必须是final的。', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-08 23:59:58', NULL, NULL);
INSERT INTO `question` VALUES (55, '4', '一个类中含有几个构造方法，称为构造方法的重载。对于重载的方法，其参数列表可以相同。', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-09 00:00:13', NULL, NULL);
INSERT INTO `question` VALUES (56, '4', '成员变量的值会因为对象的不同而不同。', NULL, NULL, NULL, NULL, '1', NULL, 3, NULL, '2019-05-09 00:00:29', NULL, NULL);
INSERT INTO `question` VALUES (65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:12:35', NULL, NULL);
INSERT INTO `question` VALUES (66, NULL, 'wer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:12:46', NULL, NULL);
INSERT INTO `question` VALUES (67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:15:16', NULL, NULL);
INSERT INTO `question` VALUES (68, NULL, 'wer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:15:23', NULL, NULL);
INSERT INTO `question` VALUES (69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:19:09', NULL, NULL);
INSERT INTO `question` VALUES (70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-24 19:19:20', NULL, NULL);
INSERT INTO `question` VALUES (71, '1', 'hello', 'f', 's', 'sf', 'ff', 'A', '备注fffffff', 1, NULL, '2020-03-27 19:29:21', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
