package com.springboot.fast.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TestController {
    @RequestMapping("/test")
    public String test(HttpServletRequest request) {
        request.setAttribute("testStr", "..............");
        return "test";
    }

    @RequestMapping("/testJson")
    @ResponseBody
    public String testBody(){
        return "{'status':'1', 'text':'欢迎来到assint-teaching测试'}";
    }

    @RequestMapping("/order/orderInfo")
    @ResponseBody
    public String orderInfo(){
        return "{'status':'1', 'text':'orderInfo'}";
    }

    @RequestMapping("/testFormCheckbox")
    public String testCheckbox(){
        return "testCheckbox";
    }
}
