package com.springboot.fast.controller.util;

import com.baidu.aip.ocr.AipOcr;
import com.baidu.aip.util.Base64Util;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;

/**
 * 百度图片转文字
 */
public class BaiduWebImageOCRUtil {
    //设置APPID/AK/SK
    public static final String APP_ID = "19873605";
    public static final String API_KEY = "SkcRB4H9z7h5LxlMI3KScFHG";
    public static final String SECRET_KEY = "s2GzMmzf9iBO507Oi1U7j4rmHL5uurNG";
    public static final AipOcr client;
    static {
        client =  new AipOcr(APP_ID, API_KEY, SECRET_KEY);
    }


    public static void main(String[] args) {

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

//        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");

        // 调用接口
        String path = "D:\\个人论文工作空间\\智能聊天机器人+在线编译器+搜题\\test.png";


        JSONObject res = client.webImage(path, new HashMap<String, String>());
        System.out.println(res.toString(2));

    }

    public static JSONObject OCRByte(byte[] data) {
        client.setConnectionTimeoutInMillis(2000);
        JSONObject res = client.webImage(data, new HashMap<String, String>());
        return res;
    }
}
