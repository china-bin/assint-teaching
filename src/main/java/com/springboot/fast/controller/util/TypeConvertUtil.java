package com.springboot.fast.controller.util;

import cn.hutool.core.convert.Convert;
import org.mockito.cglib.core.Converter;

/**
 * 类型转换
 */
public class TypeConvertUtil {

    /**
     * 将 类似 "23,12,44" 字符串转换为数组
     * @param str
     * @return
     */
    public static final Integer[] stringToIntgerArray(String str) {
        String[] strArray = str.split(",");
        return Convert.toIntArray(strArray);
    }

    public static final Long[] stringToLongArray(String str) {
        String[] strArray = str.split(",");
        return Convert.toLongArray(strArray);
    }
}
