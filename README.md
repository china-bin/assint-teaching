Java在线编译+智能对话机器人+搜题

#### 技术栈
Spring Boot、MyBatis-Plus、、thymeleaf<br/>

![license](https://img.shields.io/badge/license-MIT-yellow.svg)

### 注意事项
[similarity](https://github.com/shibing624/similarity)
由于其并没有将其jar包上传至Maven官网库。

所以这里我将jar包放在 doc文件夹下的similarity-1.1.3-jar-with-dependencies.jar

并且如下命令将jar安装至本地maven仓库中：
```java
mvn install:install-file -Dfile=doc/similarity-1.1.3-jar-with-dependencies.jar -DgroupId=io.github.shibing624 -DartifactId=similarity -Dversion=1.1.3 -Dpackaging=jar 

```
pom.xml中添加如下dpednecy:
```xml
<dependency>
	<groupId>io.github.shibing624</groupId>
	<artifactId>similarity</artifactId>
	<version>1.1.3</version>
</dependency>
```
打包命令
```java
mvn package -Dmaven.test.skip=true
```


线上服务器后台启动运行命令
```java
nohup java -jar assint-teaching-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod &
```

在线演示地址:  http://47.115.31.52:8080/front/question